import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'page-search-location',
  templateUrl: 'search-location.html'
})

export class SearchLocationPage {
  public fromto: any;

  public places = {
    nearby: [
      {
        id: 1,
        name: "Jardineiro"
      },
      {
        id: 2,
        name: "Pedreiro"
      },
      {
        id: 3,
        name: "Pintor"
      },
      {
        id: 4,
        name: "Encanador"
      }
    ],
    recent: [
      {
        id: 1,
        name: "Jardineiro"
      }
    ]
  };

  constructor(private storage: Storage, public nav: NavController, public navParams: NavParams) {
    this.fromto = this.navParams.data;
  }

  searchBy(item) {
    if (this.fromto === 'from') {
      this.storage.set('pickup', item.name);
    }

    if (this.fromto === 'to') {
      this.storage.set('dropOff', item.name);
    }

    this.nav.pop();
  }
}
